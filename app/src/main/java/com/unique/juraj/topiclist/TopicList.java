package com.unique.juraj.topiclist;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TopicList extends Activity implements AdapterView.OnItemClickListener {

    @BindView(R.id.etInput)
    EditText Input;
    @BindView(R.id.lvList)
    ListView List;
    @BindView(R.id.bAdd)
    Button Add;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_list);
        ButterKnife.bind(this);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,new ArrayList<String>());
        List.setAdapter(adapter);
        List.setOnItemClickListener(this);
    }

    @OnClick(R.id.bAdd)
    public void OnClick(View view) {
        adapter.add(Input.getText().toString());
        adapter.notifyDataSetChanged();
        Input.setText("");
//        case(R.id.lvList):
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, parent.getAdapter().getItem(position).toString());
        startActivity(intent);
//        Uri uri = Uri.parse("http://www.google.com?")
//        Log.e("LADIDA", parent.getAdapter().getItem(position).toString());
    }
}
